import numpy as np

class Point:
    def __init__(self, radius, angle):
        self.R = radius
        self.angle = angle

    def __str__(self):
        return "({}, {})".format(np.str(self.R), np.str(self.angle))
