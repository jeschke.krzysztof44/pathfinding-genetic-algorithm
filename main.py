import numpy as np
import random
from environment import World
from polar_point import Point
import matplotlib.pyplot as plt

CHROMOSOME_LEN = 200
POPULATION = 300
MAP_SIZE = 50
MUTATION_PROB = 0.1
STEPS = 500

MAX_RADIUS = 1


def unison_shuffled_copies(a, b):
    a = np.asarray(a)
    b = np.asarray(b)
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]


def cart2pol(x, y):
    radius = np.sqrt(x ** 2 + y ** 2)
    angle = np.arctan2(y, x)
    return radius, angle


def pol2cart(radius, angle):
    x = radius * np.cos(angle)
    y = radius * np.sin(angle)
    return x, y


def generate_random_individuals():
    population = []
    for i in range(POPULATION):
        chromosome = []
        for j in range(CHROMOSOME_LEN):
            angle = random.randint(0, 7) * 45
            chromosome.append(angle)

        population.append(chromosome)

    return population


def calc_distance(point_A, point_B):
    r_1 = point_A[0]
    r_2 = point_B[0]
    phi_1 = point_A[1]
    phi_2 = point_B[1]

    return np.sqrt(r_1 ** 2 + r_2 ** 2 - 2 * r_1 * r_2 * np.cos(phi_1 - phi_2))


def calc_position(moves):
    x = 0
    y = 0
    if moves == 0:
        x = 0
        y = 1
    elif moves == 45:
        x = 1
        y = 1
    elif moves == 90:
        x = 1
        y = 0
    elif moves == 135:
        x = 1
        y = -1
    elif moves == 180:
        x = 0
        y = -1
    elif moves == 225:
        x = -1
        y = -1
    elif moves == 270:
        x = -1
        y = 0
    elif moves == 315:
        x = -1
        y = 1

    return x, y


def breeding(population, scores):
    crossover_population, best_parents = crossover(population, scores)
    mutated_population = mutation(crossover_population)
    new_population = np.append(best_parents, mutated_population, axis=0)
    return new_population


def crossover(population, scores):
    best_individuals = tournament_selection(population, scores)
    best_parents_first = best_individuals[::2]
    best_parents_second = best_individuals[1::2]
    parents_population = np.array(list(zip(best_parents_first, best_parents_second)))
    child_population = []
    for parents in parents_population:
        first_parent, second_parent = parents
        first_child = np.asarray(first_parent)
        second_child = np.asarray(second_parent)

        crossover_array = np.random.rand(len(first_parent))
        crossover_array[crossover_array >= 0.5] = 1
        crossover_array[crossover_array < 0.5] = 0

        first_child[crossover_array == 0] = first_parent[crossover_array == 0]
        first_child[crossover_array == 1] = second_parent[crossover_array == 1]

        second_child[crossover_array == 0] = second_parent[crossover_array == 0]
        second_child[crossover_array == 1] = first_parent[crossover_array == 1]
        child_population.append(first_child)
        child_population.append(second_child)

    child_population = np.array(child_population)
    return child_population, best_individuals


def mutation(population):
    for individual in population:
        for i in range(CHROMOSOME_LEN):
            mutation_chance = np.random.uniform(0, 1)
            if mutation_chance < MUTATION_PROB:
                gene = np.random.randint(0, 7) * 45
                individual[i] = gene
    return population


def tournament_selection(population, scores):
    population, scores = unison_shuffled_copies(population, scores)
    winners = []
    for i in range(POPULATION // 2):
        if scores[2 * i] < scores[2 * i + 1]:
            winners.append(population[2 * i])
        else:
            winners.append(population[2 * i + 1])
    return winners


def add_individual_path_to_map(individual):
    moves = list(map(calc_position, individual))
    x_path = [i[0] for i in moves]
    y_path = [i[1] for i in moves]

    current_x = start_point[0]
    current_y = start_point[1]

    for i in range(len(x_path)):
        current_x += x_path[i]
        current_y += y_path[i]
        if not my_world.check_point(current_x, current_y):
            current_x -= x_path[i]
            current_y -= y_path[i]
            continue
        if my_world.map[current_x][current_y] == 3:
            break
        if my_world.map[current_x][current_y] == 2:
            continue
        my_world.map[current_x][current_y] = 4


def check_and_set_score(moves):
    current_x = start_point[0]
    current_y = start_point[1]
    score = 0
    fault_score = 30

    for move in moves:
        current_x += move[0]
        current_y += move[1]

        if current_x == stop_point[0] and current_y == stop_point[1]:
            return 0.0

        if not my_world.check_point(current_x, current_y):
            score += fault_score
            current_x -= move[0]
            current_y -= move[1]

    polar_stop_point = cart2pol(stop_point[0], stop_point[1])
    individual_polar_end_point = cart2pol(current_x, current_y)

    score += calc_distance(polar_stop_point, individual_polar_end_point)

    return score


def evaluate(population):
    individuals_results = []

    for individual in population:
        moves = list(map(calc_position, individual))

        individuals_results.append(check_and_set_score(moves))

    return individuals_results


my_world = World(50, obstacles_num=18, obstacle_size=0.5)
start_point, stop_point = my_world.start_point, my_world.stop_point
my_population = generate_random_individuals()

plt.figure(figsize=(10, 5))
plt.imshow(my_world.map, cmap=plt.cm.binary, interpolation='nearest')
plt.xticks([]), plt.yticks([])
plt.show()

scores = []
best_score = 1000

for i in range(STEPS):
    scores = evaluate(my_population)
    best_score = np.min(scores)
    if i % 100 == 0:
        print("Aktualny krok algorytmu: {}, najlepszy wynik: {}".format(np.str(i), np.str(best_score)))
    if best_score == 0:
        print("Trasa znaleziona w {} krokach.".format(np.str(i)))
        break
    else:
        my_population = breeding(my_population, scores)
    if i == STEPS - 1:
        print("Nie udało się znależć trasy.")

add_individual_path_to_map(my_population[scores.index(best_score)])

display_map = np.zeros((MAP_SIZE, MAP_SIZE, 3))
display_map[my_world.map == 0] = (255, 255, 255)
display_map[my_world.map == 1] = (0, 0, 0)
display_map[my_world.map == 2] = (255, 0, 0) #start_point
display_map[my_world.map == 3] = (0, 255, 0) #stop_point
display_map[my_world.map == 4] = (0, 0, 255)

plt.figure(figsize=(10, 5))
plt.imshow(display_map, interpolation='nearest')
plt.xticks([]), plt.yticks([])
plt.show()
