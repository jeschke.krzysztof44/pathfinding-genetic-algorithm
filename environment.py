import numpy as np
import random
import matplotlib.pyplot as plt


class World:
    def __init__(self, map_size, obstacles_num=10, obstacle_size=0.2):
        self.map_size = map_size
        self.obstacles_num = obstacles_num
        self.obstacle_size = np.int(obstacle_size * self.map_size)
        self.map = self.generate_map()
        self.start_point, self.stop_point = self.get_random_start_stop()

    def check_point(self, x, y):
        if x >= self.map_size or y >= self.map_size or x < 0 or y < 0:
            return False
        elif self.map[x][y] == 1:
            return False
        else:
            return True

    def get_random_start_stop(self):
        points = []

        for i in range(2):
            x = random.randint(0, self.map_size - 1)
            y = random.randint(0, self.map_size - 1)

            while not self.check_point(x, y):
                x = random.randint(0, self.map_size - 1)
                y = random.randint(0, self.map_size - 1)
            points.append((x, y))

            self.map[x][y] = i + 2

        return points

    def add_obstacles(self, my_map):
        centers = []
        for i in range(self.obstacles_num):
            x = random.randint(0, self.map_size - 1)
            y = random.randint(0, self.map_size - 1)
            centers.append((x, y))
            my_map[x][y] = 1

        for center in centers:
            for i in range(self.obstacle_size - 1):

                new_x = center[0] + random.randint(-1, 1)
                new_y = center[1] + random.randint(-1, 1)

                counter = 0
                while not self.check_point(new_x, new_y):
                    new_x = center[0] + random.randint(-1, 1)
                    new_y = center[1] + random.randint(-1, 1)
                    counter += 1

                    if counter > 10:
                        new_x = center[0]
                        new_y = center[1]
                        break

                my_map[new_x][new_y] = 1
                center = (new_x, new_y)

        return my_map

    def generate_map(self):
        self.map = np.zeros(shape=(self.map_size, self.map_size))
        self.map = self.add_obstacles(self.map)

        return self.map

# my_world = World(50, obstacles_num=18, obstacle_size=0.5)
#
# plt.figure(figsize=(10, 5))
# plt.imshow(my_world.map, cmap=plt.cm.binary, interpolation='nearest')
# plt.xticks([]), plt.yticks([])
# plt.show()
